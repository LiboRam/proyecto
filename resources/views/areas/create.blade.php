@extends('templates.master')

@section('content')

    <h2>Insertar Datos</h2>
    <hr/>
    <a class="btn btn-primary" href="/areas" style="margin-bottom: 15px;">Insertar Datos</a>

    {!! Form::open(['idArea' => 'dataForm', 'url' => '/areas']) !!}
    <div class="form-group">
        {!! Form::label('nombreArea', 'Nombre Area:'); !!}
        {!! Form::text('nombreArea', null, ['placeholder' => 'Agregar Area', 'class' => 'form-control']); !!}
    </div>

    {!! Form::submit('Enviar', ['class' => 'btn btn-primary pull-right']); !!}

    {!! Form::close() !!}
@endsection()