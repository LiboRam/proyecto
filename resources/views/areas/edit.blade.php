@extends('templates.master')

@section('content')

    <h2>Actualizar Datos</h2>
    <hr/>
    <a class="btn btn-primary" href="/areas" style="margin-bottom: 15px;">Leer datos</a>

    {!! Form::open(['idArea' => 'dataForm', 'method' => 'PATCH', 'url' => '/areas/' . $area->idArea ]) !!}
    <div class="form-group">
        {!! Form::label('nombreArea', 'Area:'); !!}
        {!! Form::text('nombreArea', $area->nombreArea , ['placeholder' => 'Agregar Area', 'class' => 'form-control']); !!}
    </div>


    {!! Form::submit('Actualizar', ['class' => 'btn btn-primary pull-right']); !!}

    {!! Form::close() !!}
@endsection()