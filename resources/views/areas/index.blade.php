@extends('templates.master')

@section('content')

    <h2>Lista areas</h2>
    <hr/>
    <a class="btn btn-primary" href="areas/create" style="margin-bottom: 15px;">Crear Nuevo</a>

    @if(Session::has('message'))
    <div class="alert-custom">
        <p>{!! Session('message') !!}</p>
    </div>
    @endif()

    <table class="table table-bordered">
        <thead>
        <tr>
            <th style="padding-left: 15px;">ID</th>
            <th>Nombre area:</th>
            <th width="110px;">Acción</th>
        </tr>
        </thead>
        <tbody>

        @foreach($areas as $area)
            <tr>
                <td style="padding-left: 15px;">{!! $area->idArea !!}</td>
                <td>{!! $area->nombreArea !!}</td>
                <td>
                    <a class="btn btn-success btn-sm" href="areas/{!! $area->idArea !!}/edit">Edit</a>

                    {!! Form::open(['idArea' => 'deleteForm', 'method' => 'DELETE', 'url' => '/areas/' . $area->idArea]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>

@endsection()