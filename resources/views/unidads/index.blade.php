@extends('templates.master')

@section('content')

    <h2>Lista Categoria</h2>
    <hr/>
    <a class="btn btn-primary" href="unidads/create" style="margin-bottom: 15px;">Crear Nuevo</a>

    @if(Session::has('message'))
    <div class="alert-custom">
        <p>{!! Session('message') !!}</p>
    </div>
    @endif()

    <table class="table table-bordered">
        <thead>
        <tr>
            <th style="padding-left: 15px;">ID</th>
            <th>Nombre unidad:</th>
            <th width="110px;">Acción</th>
        </tr>
        </thead>
        <tbody>

        @foreach($unidads as $unidad)
            <tr>
                <td style="padding-left: 15px;">{!! $unidad->idUnidad !!}</td>
                <td>{!! $unidad->nombreUnidad !!}</td>
                <td>
                    <a class="btn btn-success btn-sm" href="unidads/{!! $unidad->idUnidad !!}/edit">Edit</a>

                    {!! Form::open(['idUnidad' => 'deleteForm', 'method' => 'DELETE', 'url' => '/unidads/' . $unidad->idUnidad]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>

@endsection()