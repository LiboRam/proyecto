@extends('templates.master')

@section('content')

    <h2>Insertar Datos</h2>
    <hr/>
    <a class="btn btn-primary" href="/unidads" style="margin-bottom: 15px;">Insertar Datos</a>

    {!! Form::open(['id' => 'dataForm', 'url' => '/unidads']) !!}
    <div class="form-group">
        {!! Form::label('nombreUnidad', 'Nombre Unidad:'); !!}
        {!! Form::text('nombreUnidad', null, ['placeholder' => 'unidad de medida', 'class' => 'form-control']); !!}
    </div>

    

    {!! Form::submit('Enviar', ['class' => 'btn btn-primary pull-right']); !!}

    {!! Form::close() !!}
@endsection()