@extends('templates.master')

@section('content')

    <h2>Actualizar Datos</h2>
    <hr/>
    <a class="btn btn-primary" href="/unidads" style="margin-bottom: 15px;">Leer datos</a>

    {!! Form::open(['idUnidad' => 'dataForm', 'method' => 'PATCH', 'url' => '/unidads/' . $unidad->idUnidad ]) !!}

    <div class="form-group">
        {!! Form::label('nombreUnidad', 'Nombre Unidad:'); !!}
        {!! Form::text('nombreUnidad', $unidad->nombreUnidad , ['placeholder' => 'agregar unidad', 'class' => 'form-control']); !!}
    </div>
    

    {!! Form::submit('Actualizar', ['class' => 'btn btn-primary pull-right']); !!}

    {!! Form::close() !!}
@endsection()