@extends('templates.master')

@section('content')

    <h2>Insertar Datos</h2>
    <hr/>
    <a class="btn btn-primary" href="/solicitantes" style="margin-bottom: 15px;">Insertar Datos</a>

    {!! Form::open(['idSolicitante' => 'dataForm', 'url' => '/solicitantes']) !!}
    <div class="form-group">
        {!! Form::label('nombreSolicitante', 'Nombre:'); !!}
        {!! Form::text('nombreSolicitante', null, ['placeholder' => 'Nombre Solicitante', 'class' => 'form-control']); !!}
    </div>

    <div class="form-group">
        {!! Form::label('apellidoP', 'Apellido Paterno:'); !!}
        {!! Form::text('apellidoP', null, ['placeholder' => 'Apellido Paterno', 'class' => 'form-control']); !!}
    </div>

     <div class="form-group">
        {!! Form::label('apellidoM', 'Apellido Materno:'); !!}
        {!! Form::text('apellidoM', null, ['placeholder' => 'Apellido Materno', 'class' => 'form-control']); !!}
    </div>

    <div class="form-group">
        {!! Form::label('cargo', 'Cargo:'); !!}
        {!! Form::text('cargo', null, ['placeholder' => 'Cargo solicitante', 'class' => 'form-control']); !!}
    </div>
    
    <div class="form-group">
        {!! Form::label('dependencia', 'Dependencia:'); !!}
        {!! Form::text('dependencia', null, ['placeholder' => 'Nombre Dependencia', 'class' => 'form-control']); !!}
    </div>

     <div class="form-group">
        {!! Form::label('telefono', 'Número del Teléfono:'); !!}
        {!! Form::text('telefono', null, ['placeholder' => 'Numero tel', 'class' => 'form-control']); !!}
    </div>


    {!! Form::submit('Enviar', ['class' => 'btn btn-primary pull-right']); !!}

    {!! Form::close() !!}
@endsection()