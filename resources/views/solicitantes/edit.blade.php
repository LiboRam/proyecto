@extends('templates.master')

@section('content')

    <h2>Actualizar Datos</h2>
    <hr/>
   <a class="btn btn-primary" href="/solicitantes" style="margin-bottom: 15px;">Leer datos</a>

    {!! Form::open(['idSolicitante' => 'dataForm', 'method' => 'PATCH', 'url' => '/solicitantes/' . $solicitante->idSolicitante ]) !!}

    <div class="form-group">
        {!! Form::label('nombreSolicitante', 'Nombre:'); !!}
        {!! Form::text('nombreSolicitante', $solicitante->nombreSolicitante, ['placeholder' => 'Nombre Solicitante', 'class' => 'form-control']); !!}
    </div>

    <div class="form-group">
        {!! Form::label('apellidoP', 'Apellido Paterno:'); !!}
        {!! Form::text('apellidoP', $solicitante->apellidoP, ['placeholder' => 'Apellido Paterno', 'class' => 'form-control']); !!}
    </div>

     <div class="form-group">
        {!! Form::label('apellidoM', 'Apellido Materno:'); !!}
        {!! Form::text('apellidoM', $solicitante->apellidoM, ['placeholder' => 'Apellido Materno', 'class' => 'form-control']); !!}
    </div>

    <div class="form-group">
        {!! Form::label('cargo', 'Cargo:'); !!}
        {!! Form::text('cargo', $solicitante->cargo, ['placeholder' => 'Cargo solicitante', 'class' => 'form-control']); !!}
    </div>
    
    <div class="form-group">
        {!! Form::label('dependencia', 'Dependencia:'); !!}
        {!! Form::text('dependencia', $solicitante->dependencia, ['placeholder' => 'Nombre Dependencia', 'class' => 'form-control']); !!}
    </div>

     <div class="form-group">
        {!! Form::label('telefono', 'Número del Teléfono:'); !!}
        {!! Form::text('telefono', $solicitante->telefono, ['placeholder' => 'Numero tel', 'class' => 'form-control']); !!}
    </div>

    {!! Form::submit('Actualizar', ['class' => 'btn btn-primary pull-right']); !!}

    {!! Form::close() !!}
@endsection()