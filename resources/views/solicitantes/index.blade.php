@extends('templates.master')

@section('content')

    <h2>Lista Solicitantes</h2>
    <hr/>
    <a class="btn btn-primary" href="solicitantes/create" style="margin-bottom: 15px;">Crear Nuevo</a>

    @if(Session::has('message'))
    <div class="alert-custom">
        <p>{!! Session('message') !!}</p>
    </div>
    @endif()

    <table class="table table-bordered">
        <thead>
        <tr>
            <th style="padding-left: 15px;">ID</th>
            <th>Nombre Solicitante:</th>
            <th>Apellido Paterno:</th>
            <th>Apellido Materno:</th>
            <th>Cargo:</th>
            <th>Dependencia:</th>
            <th>Teléfono:</th>
            <th width="110px;">Acción</th>
        </tr>
        </thead>
        <tbody>

        @foreach($solicitantes as $solicitante)
            <tr>
                <td style="padding-left: 15px;">{!! $solicitante->idSolicitante !!}</td>
                <td>{!! $solicitante->nombreSolicitante !!}</td>
                <td>{!! $solicitante->apellidoP !!}</td>
                <td>{!! $solicitante->apellidoM !!}</td>
                <td>{!! $solicitante->cargo !!}</td>
                <td>{!! $solicitante->dependencia !!}</td>
                <td>{!! $solicitante->telefono !!}</td>

                <td>
                    <a class="btn btn-success btn-sm" href="solicitantes/{!! $solicitante->idSolicitante !!}/edit">Edit</a>

                    {!! Form::open(['idSolicitante ' => 'deleteForm', 'method' => 'DELETE', 'url' => '/solicitantes/' . $solicitante->idSolicitante ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>

@endsection()