@extends('templates.master')

@section('content')

    <h2>Actualizar Datos</h2>
    <hr/>
    <a class="btn btn-primary" href="/modelos" style="margin-bottom: 15px;">Leer datos</a>

    {!! Form::open(['idModelo' => 'dataForm', 'method' => 'PATCH', 'url' => '/modelos/' . $modelo->idModelo ]) !!}
    <div class="form-group">
        {!! Form::label('nombreModelo', 'Modelo:'); !!}
        {!! Form::text('nombreModelo', $modelo->nombreModelo , ['placeholder' => 'Modelo', 'class' => 'form-control']); !!}
    </div>


    {!! Form::submit('Actualizar', ['class' => 'btn btn-primary pull-right']); !!}

    {!! Form::close() !!}
@endsection()