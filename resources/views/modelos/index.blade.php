@extends('templates.master')

@section('content')

    <h2>Lista Modelos</h2>
    <hr/>
    <a class="btn btn-primary" href="modelos/create" style="margin-bottom: 15px;">Crear Nuevo</a>

    @if(Session::has('message'))
    <div class="alert-custom">
        <p>{!! Session('message') !!}</p>
    </div>
    @endif()

    <table class="table table-bordered">
        <thead>
        <tr>
            <th style="padding-left: 15px;">ID</th>
            <th>Nombre modelo:</th>
            <th width="110px;">Acción</th>
        </tr>
        </thead>
        <tbody>

        @foreach($modelos as $modelo)
            <tr>
                <td style="padding-left: 15px;">{!! $modelo->idModelo !!}</td>
                <td>{!! $modelo->nombreModelo !!}</td>
                <td>
                    <a class="btn btn-success btn-sm" href="modelos/{!! $modelo->idModelo !!}/edit">Edit</a>

                    {!! Form::open(['idModelo' => 'deleteForm', 'method' => 'DELETE', 'url' => '/modelos/' . $modelo->idModelo]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>

@endsection()