@extends('templates.master')

@section('content')

    <h2>Insertar Datos</h2>
    <hr/>
    <a class="btn btn-primary" href="/modelos" style="margin-bottom: 15px;">Insertar Datos</a>

    {!! Form::open(['idModelo' => 'dataForm', 'url' => '/modelos']) !!}
    <div class="form-group">
        {!! Form::label('nombreModelo', 'Modelo:'); !!}
        {!! Form::text('nombreModelo', null, ['placeholder' => 'Modelo', 'class' => 'form-control']); !!}
    </div>

    {!! Form::submit('Enviar', ['class' => 'btn btn-primary pull-right']); !!}

    {!! Form::close() !!}
@endsection()