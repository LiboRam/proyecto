@extends('templates.master')

@section('content')

    <h2>Insertar Datos</h2>
    <hr/>
    <a class="btn btn-primary" href="/municipios" style="margin-bottom: 15px;">Insertar Datos</a>

    {!! Form::open(['idMunicipio' => 'dataForm', 'url' => '/municipios']) !!}
    <div class="form-group">
        {!! Form::label('clave', 'Clave:'); !!}
        {!! Form::text('clave', null, ['placeholder' => 'Agregar clave', 'class' => 'form-control']); !!}
    </div>

    <div class="form-group">
        {!! Form::label('nombreMunicipio', 'Municipio:'); !!}
        {!! Form::text('nombreMunicipio', null, ['placeholder' => 'Nombre Municipio', 'class' => 'form-control']); !!}
    </div>

     <div class="form-group">
        {!! Form::label('region', 'Region:'); !!}
        {!! Form::text('region', null, ['placeholder' => 'Nombre Region', 'class' => 'form-control']); !!}
    </div>




    {!! Form::submit('Enviar', ['class' => 'btn btn-primary pull-right']); !!}

    {!! Form::close() !!}
@endsection()