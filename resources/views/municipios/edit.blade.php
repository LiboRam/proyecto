@extends('templates.master')

@section('content')

    <h2>Actualizar Datos</h2>
    <hr/>
    <a class="btn btn-primary" href="/municipios" style="margin-bottom: 15px;">Leer datos</a>

    {!! Form::open(['idMunicipio' => 'dataForm', 'method' => 'PATCH', 'url' => '/municipios/' . $municipio->idMunicipio ]) !!}

    <div class="form-group">
        {!! Form::label('clave', 'Clave:'); !!}
        {!! Form::text('clave', $municipio->clave, ['placeholder' => 'Agregar clave', 'class' => 'form-control']); !!}
    </div>

    <div class="form-group">
        {!! Form::label('nombreMunicipio', 'Municipio:'); !!}
        {!! Form::text('nombreMunicipio', $municipio->nombreMunicipio, ['placeholder' => 'Nombre Municipio', 'class' => 'form-control']); !!}
    </div>

     <div class="form-group">
        {!! Form::label('region', 'Region:'); !!}
        {!! Form::text('region', $municipio->region, ['placeholder' => 'Nombre Region', 'class' => 'form-control']); !!}
    </div>

    {!! Form::submit('Actualizar', ['class' => 'btn btn-primary pull-right']); !!}

    {!! Form::close() !!}
@endsection()