@extends('templates.master')

@section('content')

    <h2>Lista Municipios</h2>
    <hr/>
    <a class="btn btn-primary" href="municipios/create" style="margin-bottom: 15px;">Crear Nuevo</a>

    @if(Session::has('message'))
    <div class="alert-custom">
        <p>{!! Session('message') !!}</p>
    </div>
    @endif()

    <table class="table table-bordered">
        <thead>
        <tr>
            <th style="padding-left: 15px;">ID</th>
            <th>Clave:</th>
            <th>Nombre Municipio:</th>
            <th>Región:</th>
            <th width="110px;">Acción</th>
        </tr>
        </thead>
        <tbody>

        @foreach($municipios as $municipio)
            <tr>
                <td style="padding-left: 15px;">{!! $municipio->idMunicipio !!}</td>
                <td>{!! $municipio->clave !!}</td>
                <td>{!! $municipio->nombreMunicipio !!}</td>
                <td>{!! $municipio->region !!}</td>
                <td>
                    <a class="btn btn-success btn-sm" href="municipios/{!! $municipio->idMunicipio !!}/edit">Edit</a>

                    {!! Form::open(['idMunicipio' => 'deleteForm', 'method' => 'DELETE', 'url' => '/municipios/' . $municipio->idMunicipio]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>

@endsection()