@extends('templates.master')

@section('content')

    <h2>Actualizar Datos</h2>
    <hr/>
    <a class="btn btn-primary" href="/edoequipos" style="margin-bottom: 15px;">Leer datos</a>

    {!! Form::open(['idEdo' => 'dataForm', 'method' => 'PATCH', 'url' => '/edoequipos/' . $edoequipo->idEdo ]) !!}

    <div class="form-group">
        {!! Form::label('nombreEdo', 'Nombre Estado:'); !!}
        {!! Form::text('nombreEdo', $edoequipo->nombreEdo , ['placeholder' => 'agregar un estado', 'class' => 'form-control']); !!}
    </div>
    

    {!! Form::submit('Actualizar', ['class' => 'btn btn-primary pull-right']); !!}

    {!! Form::close() !!}
@endsection()