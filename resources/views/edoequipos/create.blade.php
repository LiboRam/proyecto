@extends('templates.master')

@section('content')

    <h2>Insertar Datos</h2>
    <hr/>
    <a class="btn btn-primary" href="/edoequipos" style="margin-bottom: 15px;">Insertar Datos</a>

    {!! Form::open(['idEdo' => 'dataForm', 'url' => '/edoequipos']) !!}
    <div class="form-group">
        {!! Form::label('nombreEdo', 'Nombre Estado:'); !!}
        {!! Form::text('nombreEdo', null, ['placeholder' => 'Agrega un estado', 'class' => 'form-control']); !!}
    </div>

    

    {!! Form::submit('Enviar', ['class' => 'btn btn-primary pull-right']); !!}

    {!! Form::close() !!}
@endsection()