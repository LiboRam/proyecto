@extends('templates.master')

@section('content')

    <h2>Lista Status</h2>
    <hr/>
    <a class="btn btn-primary" href="edoequipos/create" style="margin-bottom: 15px;">Crear Nuevo</a>

    @if(Session::has('message'))
    <div class="alert-custom">
        <p>{!! Session('message') !!}</p>
    </div>
    @endif()

    <table class="table table-bordered">
        <thead>
        <tr>
            <th style="padding-left: 15px;">ID</th>
            <th>Nombre Estado:</th>
            <th width="110px;">Acción</th>
        </tr>
        </thead>
        <tbody>

        @foreach($edoequipos as $edoequipo)
            <tr>
                <td style="padding-left: 15px;">{!! $edoequipo->idEdo !!}</td>
                <td>{!! $edoequipo->nombreEdo !!}</td>
                <td>
                    <a class="btn btn-success btn-sm" href="edoequipos/{!! $edoequipo->idEdo !!}/edit">Edit</a>

                    {!! Form::open(['idEdo' => 'deleteForm', 'method' => 'DELETE', 'url' => '/edoequipos/' . $edoequipo->idEdo]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>

@endsection()