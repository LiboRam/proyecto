@extends('templates.master')

@section('content')

    <h2>Actualizar Datos</h2>
    <hr/>
    <a class="btn btn-primary" href="/marcas" style="margin-bottom: 15px;">Leer datos</a>

    {!! Form::open(['idMarca' => 'dataForm', 'method' => 'PATCH', 'url' => '/marcas/' . $marca->idMarca ]) !!}
    <div class="form-group">
        {!! Form::label('nombreMarca', 'Marca:'); !!}
        {!! Form::text('nombreMarca', $marca->nombreMarca , ['placeholder' => 'Marca', 'class' => 'form-control']); !!}
    </div>


    {!! Form::submit('Actualizar', ['class' => 'btn btn-primary pull-right']); !!}

    {!! Form::close() !!}
@endsection()