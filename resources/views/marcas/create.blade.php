@extends('templates.master')

@section('content')

    <h2>Insertar Datos</h2>
    <hr/>
    <a class="btn btn-primary" href="/marcas" style="margin-bottom: 15px;">Insertar Datos</a>

    {!! Form::open(['idMarca' => 'dataForm', 'url' => '/marcas']) !!}
    <div class="form-group">
        {!! Form::label('nombreMarca', 'Marca:'); !!}
        {!! Form::text('nombreMarca', null, ['placeholder' => 'Marca', 'class' => 'form-control']); !!}
    </div>

    {!! Form::submit('Enviar', ['class' => 'btn btn-primary pull-right']); !!}

    {!! Form::close() !!}
@endsection()