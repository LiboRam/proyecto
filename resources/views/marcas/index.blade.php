@extends('templates.master')

@section('content')

    <h2>Lista Marcas</h2>
    <hr/>
    <a class="btn btn-primary" href="marcas/create" style="margin-bottom: 15px;">Crear Nuevo</a>

    @if(Session::has('message'))
    <div class="alert-custom">
        <p>{!! Session('message') !!}</p>
    </div>
    @endif()

    <table class="table table-bordered">
        <thead>
        <tr>
            <th style="padding-left: 15px;">ID</th>
            <th>Nombre marca:</th>
            <th width="110px;">Acción</th>
        </tr>
        </thead>
        <tbody>

        @foreach($marcas as $marca)
            <tr>
                <td style="padding-left: 15px;">{!! $marca->idMarca !!}</td>
                <td>{!! $marca->nombreMarca !!}</td>
                <td>
                    <a class="btn btn-success btn-sm" href="marcas/{!! $marca->idMarca !!}/edit">Edit</a>

                    {!! Form::open(['idMarca' => 'deleteForm', 'method' => 'DELETE', 'url' => '/marcas/' . $marca->idMarca]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>

@endsection()