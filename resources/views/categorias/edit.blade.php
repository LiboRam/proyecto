@extends('templates.master')

@section('content')

    <h2>Actualizar Datos</h2>
    <hr/>
    <a class="btn btn-primary" href="/categorias" style="margin-bottom: 15px;">Leer datos</a>

    {!! Form::open(['idCategoria' => 'dataForm', 'method' => 'PATCH', 'url' => '/categorias/' . $categoria->idCategoria ]) !!}

    <div class="form-group">
        {!! Form::label('nombreCategoria', 'Nombre categoria:'); !!}
        {!! Form::text('nombreCategoria', $categoria->nombreCategoria , ['placeholder' => 'agregar categoria', 'class' => 'form-control']); !!}
    </div>
    

    {!! Form::submit('Actualizar', ['class' => 'btn btn-primary pull-right']); !!}

    {!! Form::close() !!}
@endsection()