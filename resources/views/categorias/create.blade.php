@extends('templates.master')

@section('content')

    <h2>Insertar Datos</h2>
    <hr/>
    <a class="btn btn-primary" href="/categorias" style="margin-bottom: 15px;">Insertar Datos</a>

    {!! Form::open(['id' => 'dataForm', 'url' => '/categorias']) !!}
    <div class="form-group">
        {!! Form::label('nombreCategoria', 'Nombre Categoria:'); !!}
        {!! Form::text('nombreCategoria', null, ['placeholder' => 'Agrega categoria', 'class' => 'form-control']); !!}
    </div>

    

    {!! Form::submit('Enviar', ['class' => 'btn btn-primary pull-right']); !!}

    {!! Form::close() !!}
@endsection()