@extends('templates.master')

@section('content')

    <h2>Lista Categoria</h2>
    <hr/>
    <a class="btn btn-primary" href="categorias/create" style="margin-bottom: 15px;">Crear Nuevo</a>

    @if(Session::has('message'))
    <div class="alert-custom">
        <p>{!! Session('message') !!}</p>
    </div>
    @endif()

    <table class="table table-bordered">
        <thead>
        <tr>
            <th style="padding-left: 15px;">ID</th>
            <th>Nombre categoria:</th>
            <th width="110px;">Acción</th>
        </tr>
        </thead>
        <tbody>

        @foreach($categorias as $categoria)
            <tr>
                <td style="padding-left: 15px;">{!! $categoria->idCategoria !!}</td>
                <td>{!! $categoria->nombreCategoria !!}</td>
                <td>
                    <a class="btn btn-success btn-sm" href="categorias/{!! $categoria->idCategoria !!}/edit">Edit</a>

                    {!! Form::open(['idCategoria' => 'deleteForm', 'method' => 'DELETE', 'url' => '/categorias/' . $categoria->idCategoria]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>

@endsection()