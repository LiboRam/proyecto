<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('equipos', 'EquipoController');
Route::resource('marcas', 'MarcaController');
Route::resource('modelos', 'ModeloController');
Route::resource('categorias', 'CategoriaController');
Route::resource('unidads', 'UnidadController');
Route::resource('edoequipos', 'EdoEquipoController');
Route::resource('solicitantes', 'SolicitanteController');
Route::resource('municipios', 'MunicipioController');
Route::resource('ubicacions', 'UbicacionsController');
Route::resource('areas', 'AreaController');
Route::resource('usuarios', 'UsuarioController');
Route::resource('solicituds', 'SolicitudController');
Route::resource('resguardos', 'ResguardoController');
