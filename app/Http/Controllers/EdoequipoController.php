<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Edoequipo;
use Illuminate\Support\Facades\Session;


class EdoequipoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $edoequipos = Edoequipo::orderBy('idEdo')->get();
        return view('edoequipos.index',['edoequipos'=> $edoequipos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('edoequipos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = $request->all();
        Edoequipo::create($datos);

        Session::flash('message', $datos['nombreEdo'] . ' Agregado Exitosamente');
        return redirect('/edoequipos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idEdo)
    {
        $edoequipo = Edoequipo::find($idEdo);
        return view('edoequipos/edit', ['edoequipo' => $edoequipo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idEdo)
    {
        $edoequipo = Edoequipo::find($idEdo);
        $datos = $request->all();
        $edoequipo->update($datos);

        Session::flash('message', $edoequipo['nombreEdo'] . ' Actualizado Exitosamente!');
        return redirect('/edoequipos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idEdo)
    {
        $edoequipo = Edoequipo::find($idEdo);
        $edoequipo->destroy($idEdo);

        Session::flash('message', $edoequipo['nombreEdo'] . '  Eliminado correctamente!');
        return redirect('/edoequipos');
    }
}
