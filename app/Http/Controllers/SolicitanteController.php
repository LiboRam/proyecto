<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Solicitante;
use Illuminate\Support\Facades\Session;


class SolicitanteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $solicitantes = Solicitante::orderBY('idSolicitante')->get();
        return view('solicitantes.index', ['solicitantes'=> $solicitantes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('solicitantes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = $request->all();
        Solicitante::create($datos);


        Session::flash('message', $datos['nombreSolicitante'] . ' Agregado exitosamente');
        return redirect('/solicitantes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idSolicitante)
    {
        $solicitante = Solicitante::find($idSolicitante);
        return view('solicitantes/edit', ['solicitante' => $solicitante]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idSolicitante)
    {
        $solicitante = Solicitante::find($idSolicitante);
        $datos = $request->all();
        $solicitante->update($datos);

        Session::flash('message', $solicitante['nombreSolicitante'] .  ' Actualizado exitosamente');
        return redirect('/solicitantes');

       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idSolicitante)
    {
        $solicitante = Solicitante::find($idSolicitante);
        $solicitante->destroy($idSolicitante);

        Session::flash('message', $solicitante['nombreSolicitante'] . ' Eliminado correctamente');
        return redirect('/solicitantes');
    }
}
