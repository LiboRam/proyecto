<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Municipio;
use Illuminate\Support\Facades\Session;


class MunicipioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $municipios = Municipio::orderBy('idMunicipio')->get();
        return view('municipios.index',['municipios' => $municipios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('municipios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = $request->all();
        Municipio::create($datos);

        Session::flash('message', $datos['nombreMunicipio'] . ' Agregado exitosamente');
        return redirect('/municipios');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idMunicipio)
    {
       $municipio = Municipio::find($idMunicipio);
       return view('municipios/edit', ['municipio' => $municipio]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idMunicipio)
    {
        $municipio = Municipio::find($idMunicipio);
        $datos = $request->all();
        $municipio->update($datos);

        Session::flash('message', $municipio['nombreMunicipio'] . ' Actualizado exitosamente!');
        return redirect('/municipios');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idMunicipio)
    {
        $municipio = Municipio::find($idMunicipio);
        $municipio->destroy($idMunicipio);

        Session::flash('message', $municipio['nombreMunicipio'] . ' Eliminado correctamente!');
        return redirect('/municipios');
    }
}
