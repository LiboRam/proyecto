<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Marca;
use Illuminate\Support\Facades\session;
use function Psy\debug;


class MarcaController extends Controller
{
    public function index()
    {
        $marcas = Marca::orderBy('idMarca')->get();
        return view('marcas.index',['marcas' => $marcas]);
    }

    public function create()
    {
        return view('marcas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dato = $request->all();
        Marca::create($dato);

        Session::flash('message', $dato['nombreMarca'] . '  agregado exitosamente');
        return redirect('/marcas');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($idMarca)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idMarca)
    {
        $marca = Marca::find($idMarca);
        return view('marcas/edit', ['marca' => $marca]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idMarca)
    {
        $marca = Marca::find($idMarca);
        $dato = $request->all();
        $marca->update($dato);

        Session::flash('message', $marca['nombreMarca'] . ' update successfully');
        return redirect('/marcas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idMarca)
    {
        //
        $marca = Marca::find($idMarca);
        $marca->destroy($idMarca);

        Session::flash('message', $marca['nombreMarca'] . 'eliminado exitosamente');
        return redirect('/marcas');
    }
}

