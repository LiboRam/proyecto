<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use Illuminate\Support\Facades\session;
use function Psy\debug;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     
     $categorias = Categoria::orderBy('idCategoria')->get();
     return view('categorias.index',['categorias'=> $categorias]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categorias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = $request->all();
        Categoria::create($datos);

        Session::flash('message', $datos['nombreCategoria']. ' agregado exitosamente');
        return redirect('/categorias');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($idCategoria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idCategoria)
    {
        $categoria = Categoria::find($idCategoria);
        return view('categorias/edit', ['categoria' => $categoria]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idCategoria)
    {
        $categoria = Categoria::find($idCategoria);
        $datos = $request->all();
        $categoria->update($datos);

        Session::flash('message', $categoria['nombreCategoria'] .  '  Actualizado exitosamente');
        return redirect('/categorias');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idCategoria)
    {
        $categoria = Categoria::find($idCategoria);
        $categoria->destroy($idCategoria);

        Session::flash('message', $categoria['nombreCategoria'] . ' deleted successfully');
        return redirect ('/categorias');
    }
}
