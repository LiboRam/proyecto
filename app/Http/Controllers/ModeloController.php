<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modelo;
use Illuminate\Support\Facades\session;
use function Psy\debug;

class ModeloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modelos = Modelo::orderBy('idModelo')->get();
        return view('modelos.index',['modelos' => $modelos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modelos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = $request->all();
        Modelo::create($datos);

        Session::flash('message', $datos['nombreModelo'] . ' agregado exitosamente');
        return redirect('/modelos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($idModelo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idModelo)
    {
        $modelo = Modelo::find($idModelo);
        return view('modelos/edit',['modelo' => $modelo]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idModelo)
    {
        $modelo = Modelo::find($idModelo);
        $datos = $request->all();
        $modelo->update($datos);

        Session::flash('message', $modelo['nombreModelo'] .  ' update successfully');
        return redirect('/modelos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idModelo)
    {
        $modelo = Modelo::find($idModelo);
        $modelo->destroy($idModelo);

        Session::flash('message', $modelo['nombreModelo'] .  ' eliminado correctamente');
        return redirect('/modelos');
    }
}
