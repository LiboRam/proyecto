<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Unidad;
use Illuminate\Support\Facades\session;
use function Psy\debug;

class UnidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     $unidads = Unidad::orderBy('idUnidad')->get();
     return view('unidads.index',['unidads'=> $unidads]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('unidads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = $request->all();
        Unidad::create($datos);

        Session::flash('message', $datos['nombreUnidad']. 'agregado exitosamente');
        return redirect('/unidads');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($idUnidad)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idUnidad)
    {
        $unidad = Unidad::find($idUnidad);
        return view('unidads/edit', ['unidad'=> $unidad]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idUnidad)
    {

            $unidad = Unidad::find($idUnidad);
        $datos = $request->all();
        $unidad->update($datos);

        Session::flash('message', $unidad['nombreUnidad'] . '  Actualizado exitosamente');
        return redirect('/unidads');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idUnidad)
    {
        $unidad = Unidad::find($idUnidad);
        $unidad->destroy($idUnidad);

        Session::flash('message', $unidad['nombreUnidad'] . 'deleted successfully');
        return redirect ('/unidads');
    }
}
