<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Solicitante;


class Solicitante extends Model
{
    //
    protected $primaryKey ='idSolicitante';
    protected $table = 'solicitantes';
    
    protected $fillable =['nombreSolicitante','apellidoP','apellidoM','cargo','dependencia','telefono'];
}
