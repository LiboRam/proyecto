<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Area;
class Area extends Model
{
    //
    protected $primaryKey ='idArea';
    protected $table = 'areas';
    protected $fillable = ['nombreArea'];
}
