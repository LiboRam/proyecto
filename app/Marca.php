<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Marca;

class Marca extends Model
{
   
   //protected $primaryKey ='idMarca';
   protected $primaryKey ='idMarca';
   protected $table = 'marcas';
   protected $fillable = ['nombreMarca'];
}
