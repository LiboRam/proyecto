<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Unidad;

class Unidad extends Model
{
    //use Notifiable;
   
    protected $primaryKey ='idUnidad';
   protected $table = 'unidads';

   protected $fillable = ['nombreUnidad'];

}
