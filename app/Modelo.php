<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Modelo;

class Modelo extends Model
{
    //
    protected $primaryKey ='idModelo';
    protected $table = 'modelos';

    protected $fillable = ['nombreModelo'];
}
