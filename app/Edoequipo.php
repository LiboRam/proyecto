<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Edoequipo;
class Edoequipo extends Model
{
    //
  

    protected $table = 'edoequipos';
    protected $fillable = ['nombreEdo'];

    protected $primaryKey ='idEdo';
}
