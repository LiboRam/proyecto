<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Categoria;

class Categoria extends Model
{
    //
    //use Notifiable;
    protected $primaryKey ='idCategoria';
  

    protected $table = 'categorias';
    protected $fillable = ['nombreCategoria'];
}
