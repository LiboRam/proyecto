<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipo extends Model
{
    use softDeletes;
    protected $table = 'equipos';

    protected $fillable =['nombreEquipo', 'numSerie','sicipo','descripcion','fechaIngreso','foto','marca_id','modelo_id','categoria_id','unidad_id','edo_id'];
}
