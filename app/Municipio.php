<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Municipio;


class Municipio extends Model
{
	protected $primaryKey ='idMunicipio';
	protected $table = 'municipios';
	protected $fillable = ['clave','nombreMunicipio','region'];
    
}
