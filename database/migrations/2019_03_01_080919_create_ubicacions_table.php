<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUbicacionsTable extends Migration
{
    //
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ubicacions', function (Blueprint $table) {
            $table->bigIncrements('idUbicacion')->unique();
            $table->string('nombreUbicacion',40);
            $table->string('calle',40);
            $table->string('numInterior',55);
            $table->string('numExterior',55);
            $table->string('colonia',155);
            $table->string('latitud',100);
            $table->string('longitud',100);
            $table->unsignedInteger('municipio_id');
            $table->foreign('municipio_id')->references('idMunicipio')->on('municipios')->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ubicacions');
    }
}
