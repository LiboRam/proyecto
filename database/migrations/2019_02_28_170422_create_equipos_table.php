<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquiposTable extends Migration
{
    //
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipos', function (Blueprint $table) {
            $table->bigIncrements('idEquipo')->unique();
            $table->string('nombreEquipo',100);
            $table->string('numSerie',20)->notNullable();
            $table->text('descripcion');         
            $table->string('sicipo',15)->notNullable();
            $table->date('fechaIngreso')->notNullable();
            $table->string('foto',50);
            $table->unsignedInteger('marca_id');
            $table->foreign('marca_id')->references('idMarca')->on('marcas')->onDelete('cascade');
            $table->unsignedInteger('modelo_id');
           $table->foreign('modelo_id')->references('idModelo')->on('modelos')->onDelete('cascade');
           $table->unsignedInteger('categoria_id');
           $table->foreign('categoria_id')->references('idCategoria')->on('categorias')->onDelete('cascade');
           $table->unsignedInteger('unidad_id');
           $table->foreign('unidad_id')->references('idUnidad')->on('unidads')->onDelete('cascade');
           $table->unsignedInteger('edo_id');
           $table->foreign('edo_id')->references('idEdo')->on('edoequipos')->onDelete('cascade');



            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipos');
    }
}
