<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResguardoEquiposTable extends Migration
{
    //
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resguardo_equipos', function (Blueprint $table) {
            $table->unsignedInteger('equipo_id');
            $table->foreign('equipo_id')->references('idEquipo')->on('equipos')->onDelete('cascade');
            $table->string('cantidadEntregado',4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resguardo_equipos');
    }
}
